import App from './App.svelte';

const app = new App({
	target: document.body,
	// props: {
	// 	name: 'world'
	// }
});

// Grab elements, create settings, etc.
var video = document.getElementById('video');

console.log(navigator);
// Get access to the camera!
if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
	// Not adding `{ audio: true }` since we only want video now
	navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
		//video.src = window.URL.createObjectURL(stream);
		video.srcObject = stream;
		video.play();
	});
}

export default app;